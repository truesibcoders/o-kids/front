import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Registration from './views/Registration.vue'
import Events from './views/Events.vue'
import EventView from './views/EventView.vue'
import Programs from './views/Programs.vue'
import ProgramView from './views/ProgramView.vue'
import Recruitment from './views/Recruitment.vue'
import Profile from './views/Profile.vue'
import Game from './views/Game.vue'
import Page404 from './views/Page404.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/events',
      name: 'Events',
      component: Events
    },
    {
      path: '/events/:type',
      name: 'EventsType',
      component: Events
    },
    {
      path: '/events/:id',
      name: 'EventView',
      component: EventView
    },
    {
      path: '/programs',
      name: 'Programs',
      component: Programs
    },
    {
      path: '/programs/:type',
      name: 'ProgramsType',
      component: Programs
    },
    {
      path: '/programs/:id',
      name: 'ProgramView',
      component: ProgramView
    },
    {
      path: '/recruitment',
      name: 'Recruitment',
      component: Recruitment,
      meta: { requiresAuth: true }
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: { requiresAuth: true }
    },
    {
      path: '/game',
      name: 'Game',
      component: Game,
      meta: { requiresAuth: true }
    },
    {
      path: '*',
      name: '404',
      component: Page404
    }
  ]
})

router.beforeEach((to, from, next) => {
  const fio = localStorage.getItem('fio')
  const id = localStorage.getItem('id')
  const token = localStorage.getItem('token')
  if (fio && id && token) {
    store.commit('setUser', { fio, id, token })
    next()
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.authorized) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else next()
  } else next()
})

export default router

import Vue from 'vue'
import Vuex from 'vuex'
/* global axios:true */

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      name: ''
    },
    events: [],
    programs: [],
    authorized: false,
    menu: [
      {
        name: 'Главная',
        link: 'Home',
        needAuth: false
      },
      {
        name: 'Образовательные программы',
        link: 'Programs',
        needAuth: false
      },
      {
        name: 'Мероприятия',
        link: 'Events',
        needAuth: false
      },
      {
        name: 'Подбор специалиста',
        link: 'Recruitment',
        needAuth: true
      }
    ]
  },
  mutations: {
    setUser (state, user) {
      const { name, surname, patronymic, token, id, fio } = user
      const fullname = fio || `${surname} ${name[0]}. ${patronymic[0]}.`
      state.user.name = fullname
      state.authorized = true
      localStorage.setItem('token', token)
      localStorage.setItem('fio', fullname)
      localStorage.setItem('id', id)
    },
    logout (state) {
      state.user.name = ''
      state.authorized = false
      localStorage.removeItem('token')
      localStorage.removeItem('fio')
      localStorage.removeItem('id')
    },
    setAuth (state) {
      state.authorized = true
    },
    setEvents (state, events) {
      state.events = events
    },
    setPrograms (state, programs) {
      state.programs = programs
    }
  },
  actions: {
    async login ({ commit }, { username, password }) {
      try {
        const response = await axios.post('/mentor/get', { username, password })
        commit('setUser', response.data.mentor)
      } catch (e) {
        throw e
      }
    },
    async registration ({ commit }, user) {
      try {
        const response = await axios.post('/account/create', user)
        commit('setUser', response.data.mentor)
      } catch (e) {
        throw e
      }
    },
    logout ({ commit }) {
      commit('logout')
    },
    async getEvents ({ commit }) {
      if (!this.getters.events.length) {
        try {
          const response = await axios.post('/event/getAll')
          commit('setEvents', response.data.events)
        } catch (e) {
          throw e
        }
      } else {
        commit('setEvents', this.getters.events)
      }
    },
    async getPrograms ({ commit }) {
      if (!this.getters.programs.length) {
        try {
          const response = await axios.post('/program/getAll')
          commit('setPrograms', response.data.programs)
        } catch (e) {
          throw e
        }
      } else {
        commit('setPrograms', this.getters.programs)
      }
    },
    async getUser ({ commit }) {
      try {
        const response = await axios.post('/profile/get')
        return response.data.mentor
      } catch (e) {
        throw e
      }
    }
  },
  getters: {
    user: state => state.user,
    authorized: state => state.authorized,
    menu: state => state.menu,
    events: state => state.events,
    programs: state => state.programs
  }
})
